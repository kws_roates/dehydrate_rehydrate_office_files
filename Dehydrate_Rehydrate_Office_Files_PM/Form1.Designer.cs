﻿
namespace Dehydrate_Rehydrate_Office_Files_PM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.MainOptionGroup = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.OptionRehydrate = new System.Windows.Forms.RadioButton();
            this.OptionDehydrate = new System.Windows.Forms.RadioButton();
            this.ActionGroup = new System.Windows.Forms.GroupBox();
            this.ActionKeepNOImages = new System.Windows.Forms.RadioButton();
            this.ActionKeepSOMEImages = new System.Windows.Forms.RadioButton();
            this.ActionKeepALLImages = new System.Windows.Forms.RadioButton();
            this.SelectDehydrateFile = new System.Windows.Forms.Button();
            this.SelectOrgSrcFile = new System.Windows.Forms.Button();
            this.SelectFileWithTranslations = new System.Windows.Forms.Button();
            this.DehydrateButton = new System.Windows.Forms.Button();
            this.RehydrateButton = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.PathReduce = new System.Windows.Forms.TextBox();
            this.PathOrginal = new System.Windows.Forms.TextBox();
            this.PathTranslated = new System.Windows.Forms.TextBox();
            this.QualityGroup = new System.Windows.Forms.GroupBox();
            this.QualityUgly = new System.Windows.Forms.RadioButton();
            this.QualityBad = new System.Windows.Forms.RadioButton();
            this.QualityGood = new System.Windows.Forms.RadioButton();
            this.MainOptionGroup.SuspendLayout();
            this.ActionGroup.SuspendLayout();
            this.QualityGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainOptionGroup
            // 
            this.MainOptionGroup.Controls.Add(this.label2);
            this.MainOptionGroup.Controls.Add(this.label1);
            this.MainOptionGroup.Controls.Add(this.OptionRehydrate);
            this.MainOptionGroup.Controls.Add(this.OptionDehydrate);
            this.MainOptionGroup.Location = new System.Drawing.Point(31, 24);
            this.MainOptionGroup.Name = "MainOptionGroup";
            this.MainOptionGroup.Size = new System.Drawing.Size(864, 84);
            this.MainOptionGroup.TabIndex = 0;
            this.MainOptionGroup.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(458, -3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rehydrate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, -3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Dehydrate";
            // 
            // OptionRehydrate
            // 
            this.OptionRehydrate.AutoSize = true;
            this.OptionRehydrate.Location = new System.Drawing.Point(461, 36);
            this.OptionRehydrate.Name = "OptionRehydrate";
            this.OptionRehydrate.Size = new System.Drawing.Size(365, 21);
            this.OptionRehydrate.TabIndex = 1;
            this.OptionRehydrate.Text = "I am restoring the original image sizes to a translation";
            this.OptionRehydrate.UseVisualStyleBackColor = true;
            this.OptionRehydrate.CheckedChanged += new System.EventHandler(this.OptionRehydrate_CheckedChanged);
            // 
            // OptionDehydrate
            // 
            this.OptionDehydrate.AutoSize = true;
            this.OptionDehydrate.Checked = true;
            this.OptionDehydrate.Location = new System.Drawing.Point(19, 36);
            this.OptionDehydrate.Name = "OptionDehydrate";
            this.OptionDehydrate.Size = new System.Drawing.Size(327, 21);
            this.OptionDehydrate.TabIndex = 0;
            this.OptionDehydrate.TabStop = true;
            this.OptionDehydrate.Text = "I am reducing the images in a file for translation";
            this.OptionDehydrate.UseVisualStyleBackColor = true;
            this.OptionDehydrate.CheckedChanged += new System.EventHandler(this.OptionDehydrate_CheckedChanged);
            // 
            // ActionGroup
            // 
            this.ActionGroup.Controls.Add(this.ActionKeepNOImages);
            this.ActionGroup.Controls.Add(this.ActionKeepSOMEImages);
            this.ActionGroup.Controls.Add(this.ActionKeepALLImages);
            this.ActionGroup.Location = new System.Drawing.Point(31, 221);
            this.ActionGroup.Name = "ActionGroup";
            this.ActionGroup.Size = new System.Drawing.Size(403, 133);
            this.ActionGroup.TabIndex = 1;
            this.ActionGroup.TabStop = false;
            this.ActionGroup.Text = "Action";
            // 
            // ActionKeepNOImages
            // 
            this.ActionKeepNOImages.AutoSize = true;
            this.ActionKeepNOImages.Location = new System.Drawing.Point(19, 90);
            this.ActionKeepNOImages.Name = "ActionKeepNOImages";
            this.ActionKeepNOImages.Size = new System.Drawing.Size(152, 21);
            this.ActionKeepNOImages.TabIndex = 2;
            this.ActionKeepNOImages.Text = "All images removed";
            this.ActionKeepNOImages.UseVisualStyleBackColor = true;
            this.ActionKeepNOImages.CheckedChanged += new System.EventHandler(this.ActionKeepNOImages_CheckedChanged);
            // 
            // ActionKeepSOMEImages
            // 
            this.ActionKeepSOMEImages.AutoSize = true;
            this.ActionKeepSOMEImages.Checked = true;
            this.ActionKeepSOMEImages.Location = new System.Drawing.Point(19, 63);
            this.ActionKeepSOMEImages.Name = "ActionKeepSOMEImages";
            this.ActionKeepSOMEImages.Size = new System.Drawing.Size(219, 21);
            this.ActionKeepSOMEImages.TabIndex = 1;
            this.ActionKeepSOMEImages.TabStop = true;
            this.ActionKeepSOMEImages.Text = "Some images maybe removed";
            this.ActionKeepSOMEImages.UseVisualStyleBackColor = true;
            this.ActionKeepSOMEImages.CheckedChanged += new System.EventHandler(this.ActionKeepSOMEImages_CheckedChanged);
            // 
            // ActionKeepALLImages
            // 
            this.ActionKeepALLImages.AutoSize = true;
            this.ActionKeepALLImages.Location = new System.Drawing.Point(19, 36);
            this.ActionKeepALLImages.Name = "ActionKeepALLImages";
            this.ActionKeepALLImages.Size = new System.Drawing.Size(129, 21);
            this.ActionKeepALLImages.TabIndex = 0;
            this.ActionKeepALLImages.Text = "Keep all images";
            this.ActionKeepALLImages.UseVisualStyleBackColor = true;
            this.ActionKeepALLImages.CheckedChanged += new System.EventHandler(this.ActionKeepALLImages_CheckedChanged);
            // 
            // SelectDehydrateFile
            // 
            this.SelectDehydrateFile.Location = new System.Drawing.Point(109, 120);
            this.SelectDehydrateFile.Name = "SelectDehydrateFile";
            this.SelectDehydrateFile.Size = new System.Drawing.Size(222, 32);
            this.SelectDehydrateFile.TabIndex = 2;
            this.SelectDehydrateFile.Text = "Select a file to reduce";
            this.SelectDehydrateFile.UseVisualStyleBackColor = true;
            this.SelectDehydrateFile.Click += new System.EventHandler(this.SelectDehydrateFile_Click);
            // 
            // SelectOrgSrcFile
            // 
            this.SelectOrgSrcFile.Location = new System.Drawing.Point(579, 120);
            this.SelectOrgSrcFile.Name = "SelectOrgSrcFile";
            this.SelectOrgSrcFile.Size = new System.Drawing.Size(222, 32);
            this.SelectOrgSrcFile.TabIndex = 3;
            this.SelectOrgSrcFile.Text = "Select original hand off file";
            this.SelectOrgSrcFile.UseVisualStyleBackColor = true;
            this.SelectOrgSrcFile.Click += new System.EventHandler(this.SelectOrgSrcFile_Click);
            // 
            // SelectFileWithTranslations
            // 
            this.SelectFileWithTranslations.Location = new System.Drawing.Point(579, 297);
            this.SelectFileWithTranslations.Name = "SelectFileWithTranslations";
            this.SelectFileWithTranslations.Size = new System.Drawing.Size(222, 32);
            this.SelectFileWithTranslations.TabIndex = 4;
            this.SelectFileWithTranslations.Text = "Select the file with translations";
            this.SelectFileWithTranslations.UseVisualStyleBackColor = true;
            this.SelectFileWithTranslations.Click += new System.EventHandler(this.SelectFileWithTranslations_Click);
            // 
            // DehydrateButton
            // 
            this.DehydrateButton.Location = new System.Drawing.Point(149, 451);
            this.DehydrateButton.Name = "DehydrateButton";
            this.DehydrateButton.Size = new System.Drawing.Size(152, 57);
            this.DehydrateButton.TabIndex = 5;
            this.DehydrateButton.Text = "Dehydrate!";
            this.DehydrateButton.UseVisualStyleBackColor = true;
            this.DehydrateButton.Click += new System.EventHandler(this.DehydrateButton_Click);
            // 
            // RehydrateButton
            // 
            this.RehydrateButton.Location = new System.Drawing.Point(616, 451);
            this.RehydrateButton.Name = "RehydrateButton";
            this.RehydrateButton.Size = new System.Drawing.Size(147, 60);
            this.RehydrateButton.TabIndex = 6;
            this.RehydrateButton.Text = "Rehydrate!";
            this.RehydrateButton.UseVisualStyleBackColor = true;
            this.RehydrateButton.Click += new System.EventHandler(this.RehydrateButton_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(31, 525);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(864, 33);
            this.progressBar.TabIndex = 7;
            // 
            // PathReduce
            // 
            this.PathReduce.Location = new System.Drawing.Point(33, 171);
            this.PathReduce.Name = "PathReduce";
            this.PathReduce.Size = new System.Drawing.Size(401, 22);
            this.PathReduce.TabIndex = 8;
            // 
            // PathOrginal
            // 
            this.PathOrginal.Location = new System.Drawing.Point(492, 171);
            this.PathOrginal.Name = "PathOrginal";
            this.PathOrginal.Size = new System.Drawing.Size(403, 22);
            this.PathOrginal.TabIndex = 9;
            // 
            // PathTranslated
            // 
            this.PathTranslated.Location = new System.Drawing.Point(492, 347);
            this.PathTranslated.Name = "PathTranslated";
            this.PathTranslated.Size = new System.Drawing.Size(403, 22);
            this.PathTranslated.TabIndex = 10;
            // 
            // QualityGroup
            // 
            this.QualityGroup.Controls.Add(this.QualityUgly);
            this.QualityGroup.Controls.Add(this.QualityBad);
            this.QualityGroup.Controls.Add(this.QualityGood);
            this.QualityGroup.Location = new System.Drawing.Point(33, 368);
            this.QualityGroup.Name = "QualityGroup";
            this.QualityGroup.Size = new System.Drawing.Size(401, 65);
            this.QualityGroup.TabIndex = 11;
            this.QualityGroup.TabStop = false;
            this.QualityGroup.Text = "Image Quality";
            // 
            // QualityUgly
            // 
            this.QualityUgly.AutoSize = true;
            this.QualityUgly.Location = new System.Drawing.Point(273, 32);
            this.QualityUgly.Name = "QualityUgly";
            this.QualityUgly.Size = new System.Drawing.Size(100, 21);
            this.QualityUgly.TabIndex = 2;
            this.QualityUgly.Text = "Low (144p)";
            this.QualityUgly.UseVisualStyleBackColor = true;
            this.QualityUgly.CheckedChanged += new System.EventHandler(this.QualityUgly_CheckedChanged);
            // 
            // QualityBad
            // 
            this.QualityBad.AutoSize = true;
            this.QualityBad.Checked = true;
            this.QualityBad.Location = new System.Drawing.Point(133, 32);
            this.QualityBad.Name = "QualityBad";
            this.QualityBad.Size = new System.Drawing.Size(134, 21);
            this.QualityBad.TabIndex = 1;
            this.QualityBad.TabStop = true;
            this.QualityBad.Text = "Balanced (240p)";
            this.QualityBad.UseVisualStyleBackColor = true;
            this.QualityBad.CheckedChanged += new System.EventHandler(this.QualityBad_CheckedChanged);
            // 
            // QualityGood
            // 
            this.QualityGood.AutoSize = true;
            this.QualityGood.Location = new System.Drawing.Point(17, 32);
            this.QualityGood.Name = "QualityGood";
            this.QualityGood.Size = new System.Drawing.Size(110, 21);
            this.QualityGood.TabIndex = 0;
            this.QualityGood.Text = "Good (480p)";
            this.QualityGood.UseVisualStyleBackColor = true;
            this.QualityGood.CheckedChanged += new System.EventHandler(this.QualityGood_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 594);
            this.Controls.Add(this.QualityGroup);
            this.Controls.Add(this.PathTranslated);
            this.Controls.Add(this.PathOrginal);
            this.Controls.Add(this.PathReduce);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.RehydrateButton);
            this.Controls.Add(this.DehydrateButton);
            this.Controls.Add(this.SelectFileWithTranslations);
            this.Controls.Add(this.SelectOrgSrcFile);
            this.Controls.Add(this.SelectDehydrateFile);
            this.Controls.Add(this.ActionGroup);
            this.Controls.Add(this.MainOptionGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Dehydrate & Rehydrate";
            this.MainOptionGroup.ResumeLayout(false);
            this.MainOptionGroup.PerformLayout();
            this.ActionGroup.ResumeLayout(false);
            this.ActionGroup.PerformLayout();
            this.QualityGroup.ResumeLayout(false);
            this.QualityGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox MainOptionGroup;
        private System.Windows.Forms.RadioButton OptionRehydrate;
        private System.Windows.Forms.RadioButton OptionDehydrate;
        private System.Windows.Forms.GroupBox ActionGroup;
        private System.Windows.Forms.RadioButton ActionKeepNOImages;
        private System.Windows.Forms.RadioButton ActionKeepSOMEImages;
        private System.Windows.Forms.RadioButton ActionKeepALLImages;
        private System.Windows.Forms.Button SelectDehydrateFile;
        private System.Windows.Forms.Button SelectOrgSrcFile;
        private System.Windows.Forms.Button SelectFileWithTranslations;
        private System.Windows.Forms.Button DehydrateButton;
        private System.Windows.Forms.Button RehydrateButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox PathReduce;
        private System.Windows.Forms.TextBox PathOrginal;
        private System.Windows.Forms.TextBox PathTranslated;
        private System.Windows.Forms.GroupBox QualityGroup;
        private System.Windows.Forms.RadioButton QualityUgly;
        private System.Windows.Forms.RadioButton QualityBad;
        private System.Windows.Forms.RadioButton QualityGood;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

