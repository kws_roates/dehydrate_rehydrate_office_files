﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using System.Xml;

namespace Dehydrate_Rehydrate_Office_Files_PM
{
    public partial class Form1 : Form
    {
        BackgroundWorker dehydrate_oWorker;
        BackgroundWorker rehydrate_oWorker;
        public Form1()
        {
            InitializeComponent();

            SelectDehydrateFile.Enabled = true;
            SelectOrgSrcFile.Enabled = false;
            SelectFileWithTranslations.Enabled = false;
            PathReduce.Enabled = true;
            DehydrateButton.Enabled = true;

            ActionGroup.Enabled = true;
            QualityGroup.Enabled = true;

            PathOrginal.Enabled = false;
            PathTranslated.Enabled = false;
            RehydrateButton.Enabled = false;

            dehydrate_oWorker = new BackgroundWorker();
            dehydrate_oWorker.DoWork += new DoWorkEventHandler(Dehydrate_oWorker_DoWork);
            dehydrate_oWorker.ProgressChanged += new ProgressChangedEventHandler(Dehydrate_ProgressChanged);
            dehydrate_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Dehydrate_Completed);
            dehydrate_oWorker.WorkerReportsProgress = true;
            dehydrate_oWorker.WorkerSupportsCancellation = false;

            rehydrate_oWorker = new BackgroundWorker();
            rehydrate_oWorker.DoWork += new DoWorkEventHandler(Rehydrate_oWorker_DoWork);
            rehydrate_oWorker.ProgressChanged += new ProgressChangedEventHandler(Rehydrate_ProgressChanged);
            rehydrate_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Rehydrate_Completed);
            rehydrate_oWorker.WorkerReportsProgress = true;
            rehydrate_oWorker.WorkerSupportsCancellation = false;
        }

        //Dehydration Worker fuctions
        private void Dehydrate_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int imagecount = 0;

            ImageSettings imageSettings = new ImageSettings
            {
                size = 1,
                compression_gif = 1,
                compression_jpg = 1,
                compression_png = 1,
                compression_tif = 1
            };

            string app = "";

            string @filename = PathReduce.Text;
            filename = filename.Replace("\"", "");

            string DirectoryName = new FileInfo(filename).DirectoryName;
            string AbsPathTofile = new FileInfo(filename).FullName;
            string NameOfFile = new FileInfo(filename).Name;
            string FileExt = new FileInfo(filename).Extension;
            string UnzipDestinationDir = AbsPathTofile.Replace(FileExt, "");

            bool createReport = false;
            StringBuilder report = new StringBuilder();
            report.AppendLine("Could not replace these media:");

            Debug.WriteLine($"abspath: {AbsPathTofile}\nDirectory: {DirectoryName}\nNameOfFile: {NameOfFile}\nFileExt: {FileExt}\nUnzip Directory: {UnzipDestinationDir}");


            if (QualityGood.Checked == true)
            {
                imageSettings.size = 480;
                imageSettings.compression_gif = 60;
                imageSettings.compression_jpg = 85;
                imageSettings.compression_png = 60;
                imageSettings.compression_tif = 60;
            }
            if (QualityBad.Checked == true)
            {
                //TODO: Play with these settings a bit more
                //a good balance between good and ugly
                imageSettings.size = 240;
                imageSettings.compression_gif = 50;
                imageSettings.compression_jpg = 75;
                imageSettings.compression_png = 50;
                imageSettings.compression_tif = 45;
            }
            if (QualityUgly.Checked == true)
            {
                //ah man this will look ugly as hell for sure
                imageSettings.size = 144;
                imageSettings.compression_gif = 60;
                imageSettings.compression_jpg = 87;
                imageSettings.compression_png = 60;
                imageSettings.compression_tif = 60;
            }

            Debug.WriteLine($"Quality\nGood: {QualityGood.Checked}\nBad: {QualityBad.Checked}\nUgly: {QualityUgly.Checked}\nSize: {imageSettings.size}");
            Debug.WriteLine($"gif: {imageSettings.compression_gif}\njpg: {imageSettings.compression_jpg}\npng: {imageSettings.compression_png}\ntif: {imageSettings.compression_tif}");

            switch (FileExt.ToLower())
            {

                case ".pptx":
                    app = "ppt";
                    break;
                case ".xlsx":
                    app = "xl";
                    break;
                case ".xlsm":
                    app = "xl";
                    break;
                case ".docx":
                    app = "word";
                    break;
                default:
                    break;
            }

            Debug.WriteLine($"Media to process: {app}");

            //check if the directory exists
            if (Directory.Exists(UnzipDestinationDir) == true)
            {
                Debug.WriteLine("Unzip Destination Directory Exists!");
                Directory.Delete(UnzipDestinationDir, true);
                Debug.WriteLine("Removed!");
            }

            ZipFile.ExtractToDirectory(AbsPathTofile, UnzipDestinationDir);


            //process the media directory
            string[] allMedia = Directory.GetFiles(Path.Combine(UnzipDestinationDir, app, "media"));

            for (int i = 0; i < allMedia.Count(); i++)
            {
                bool replacedUnknownMedia = true;

                Debug.WriteLine($"{allMedia[i]}");
                //known endings
                string ImgFileExt = new FileInfo(allMedia[i]).Extension;

                switch (ImgFileExt.ToLower())
                {
                    //Known image types we will always compress these
                    case ".gif":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;
                    case ".jpg":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;
                    case ".jpeg":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;
                    case ".jfif":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;
                    case ".png":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;
                    case ".tif":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;
                    case ".tiff":
                        ProcessWellKnownMedia(allMedia[i], imageSettings);
                        break;

                    //known audio and video media types we will replace these with a truncated version
                    case ".mov":
                        MakeBlankMovie(allMedia[i]);
                        break;
                    case ".mp3":
                        MakeBlankMusic(allMedia[i]);
                        break;
                    case ".mp4":
                        MakeBlankMovie(allMedia[i]);
                        break;
                    case ".m4v":
                        MakeBlankMovie(allMedia[i]);
                        break;
                    case ".wav":
                        MakeBlankMusic(allMedia[i]);
                        break;

                    //Special formats that have run into that we can't easily compress so we will replace these
                    case ".emf":
                        ProcessLesserKnownMedia(allMedia[i]);
                        break;
                    case ".wmf":
                        ProcessLesserKnownMedia(allMedia[i]);
                        break;
                    case ".svg":
                        ProcessLesserKnownMedia(allMedia[i]);
                        break;


                    //And if we don't know we will do our best to replace
                    default:
                        replacedUnknownMedia = ProcessUnKnownMedia(allMedia[i]);
                        if (replacedUnknownMedia == false)
                        {
                            report.AppendLine($"{new FileInfo(allMedia[i]).Name}");
                        }
                        break;

                }

                //we will trigger the report if we encountered an unknown file type that we failed to replace or ignore 
                if (replacedUnknownMedia == false)
                {
                    createReport = true;
                }

                imagecount++;
                dehydrate_oWorker.ReportProgress(Percentage(imagecount,allMedia.Count()));

            }

            //create a directory
            if (Directory.Exists(Path.Combine(DirectoryName, "Done")) == false)
            {
                Directory.CreateDirectory(Path.Combine(DirectoryName, "Done"));
            }

            //create a report (optional)
            if (createReport == true)
            {
                File.WriteAllText(Path.Combine(DirectoryName, "Done", "report.txt"), report.ToString());
            }

            string action = "";
            string quality = "";
            if (ActionKeepALLImages.Checked == true)
            {
                action = "[Reduced]_";
            }
            if (ActionKeepSOMEImages.Checked == true)
            {
                action = "[Extremely_Reduced]_";
            }
            if (ActionKeepNOImages.Checked == true)
            {
                action = "[Images_Removed]_";
            }

            if (QualityGood.Checked == true)
            {
                if (ActionKeepNOImages.Checked == false)
                {
                    quality = "[High_Quality]_";
                }

            }

            if (QualityBad.Checked == true)
            {
                if (ActionKeepNOImages.Checked == false)
                {
                    quality = "[Medium_Quality]_";
                }

            }

            if (QualityUgly.Checked == true)
            {
                if (ActionKeepNOImages.Checked == false)
                {
                    quality = "[Minimum_Quality]_";
                }

            }



            //Incase things blewup we may need to remove a previous fail
            if (File.Exists(Path.Combine(DirectoryName, $"{action}{quality}{NameOfFile}")) == true)
            {
                File.Delete(Path.Combine(DirectoryName, $"{action}{quality}{NameOfFile}"));
            }

            //zip the file
            string trgfile = Path.Combine(DirectoryName, $"{action}{quality}{NameOfFile}");

            ZipFile.CreateFromDirectory(UnzipDestinationDir, trgfile);

            if (File.Exists(Path.Combine(DirectoryName, "Done", $"{action}{quality}{NameOfFile}")) == true)
            {
                File.Delete(Path.Combine(DirectoryName, "Done", $"{action}{quality}{NameOfFile}"));
            }

            File.Move(trgfile, Path.Combine(DirectoryName, "Done", $"{action}{quality}{NameOfFile}"));

            //Delete the directory we unzipped
            Directory.Delete(UnzipDestinationDir, true);


        }

        private void Dehydrate_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        void Dehydrate_Completed(object sender, RunWorkerCompletedEventArgs e)
        {

            MessageBox.Show("Done");
            progressBar.Value = 0;

            MainOptionGroup.Enabled = true;
            SelectDehydrateFile.Enabled = true;
            SelectOrgSrcFile.Enabled = false;
            SelectFileWithTranslations.Enabled = false;
            PathReduce.Enabled = true;
            DehydrateButton.Enabled = true;


            ActionGroup.Enabled = true;

            if (ActionKeepNOImages.Checked == true)
            {
                QualityGroup.Enabled = false;
            }
            else
            {
                QualityGroup.Enabled = true;
            }

            PathOrginal.Enabled = false;
            PathTranslated.Enabled = false;
            RehydrateButton.Enabled = false;

            string @filename = PathReduce.Text;
            string Absfilename = filename.Replace("\"", "");
            string directoryName = new FileInfo(Absfilename).DirectoryName;

            Process.Start(Path.Combine(directoryName, "Done"));



        }

        //Dehydration ulitity fuctions

        private void ProcessWellKnownMedia(string image, ImageSettings imageSettings)
        {
            int geometry = imageSettings.size;
            MagickGeometry size = new MagickGeometry(0, geometry);
            MagickImageInfo img = new MagickImageInfo(image);
            size.IgnoreAspectRatio = false;

            if (ActionKeepNOImages.Checked == true)
            {
                //replace the image with a 1px replacement
                Debug.WriteLine($"Replacing Image {image}");
                File.Delete(image);
                MakeBlankFile(image);
            }
            else
            {
                //gif
                if (img.Format.ToString() == "Gif")
                {
                    MagickImageCollection gif = new MagickImageCollection(image);
                    gif.Coalesce();

                    if (img.Width > geometry || img.Height > geometry)
                    {
                        gif[0].Resize(size);
                    }

                    gif[0].Write(image, MagickFormat.Gif);

                }
                //jpg
                if (img.Format.ToString() == "Jpeg")
                {
                    MagickImage jpg = new MagickImage(image);

                    if (img.Width > geometry || img.Height > geometry)
                    {
                        jpg.Resize(size);
                    }
                    jpg.Quality = imageSettings.compression_jpg;
                    jpg.Write(image, MagickFormat.Jpeg);
                }
                //png
                if (img.Format.ToString() == "Png")
                {
                    MagickImage png = new MagickImage(image);

                    if (img.Width > geometry || img.Height > geometry)
                    {
                        png.Resize(size);
                    }
                    png.Quality = imageSettings.compression_png;
                    png.Write(image, MagickFormat.Png8);

                }
                //tif
                if (img.Format.ToString() == "Tiff")
                {
                    MagickImage tif = new MagickImage(image);

                    if (img.Width > geometry || img.Height > geometry)
                    {
                        tif.Resize(size);
                    }
                    tif.Quality = imageSettings.compression_tif;
                    tif.Write(image, MagickFormat.Tiff);
                }

            }

        }

        private void ProcessLesserKnownMedia(string image)
        {

            if (ActionKeepALLImages.Checked == true)
            {
                Debug.WriteLine($"Ignoring Image {image}");
            }
            else
            {
                //replace the image with a 1px replacement
                Debug.WriteLine($"Replacing Image {image}");




                if (image.ToLower().EndsWith(".emf"))
                {
                    if (File.Exists(image))
                    {

                        File.Delete(image);
                        Metafile mf = MakeMetafile(12, 12, image);
                        DrawOnMetafile(mf);
                        mf.Dispose();
                    }

                }


                if (image.ToLower().EndsWith(".wmf"))
                {
                    if (File.Exists(image))
                    {

                        File.Delete(image);
                        Metafile mf = MakeMetafile(12, 12, image);
                        DrawOnMetafile(mf);
                        mf.Dispose();
                    }

                }

                if (image.ToLower().Contains(".svg"))
                {
                    if (File.Exists(image))
                    {
                        XmlDocument docSource = new XmlDocument();
                        XmlWriterSettings settings = new XmlWriterSettings
                        {
                            Indent = false,
                            CloseOutput = true
                        };

                        docSource.Load(image);
                        XmlNodeList svg_root = docSource.GetElementsByTagName("svg");
                        foreach (XmlNode svg_child in svg_root)
                        {

                            for (int node = svg_child.ChildNodes.Count - 1; node >= 0; node--)
                            {
                                svg_child.RemoveChild(svg_child.ChildNodes[node]);
                            }

                        }

                        docSource.Save(image);

                    }

                }

            }

        }

        private bool ProcessUnKnownMedia(string image)
        {

            if (ActionKeepALLImages.Checked == true)
            {
                Debug.WriteLine($"Ignoring Image {image}");
                return true;
            }
            else
            {
                //find the image ending
                string fileExt = new FileInfo(image).Extension;

                if (File.Exists(@".\Media\image" + fileExt.ToLower()))
                {

                    File.Delete(image);
                    File.Copy(@".\Media\image" + fileExt.ToLower(), image);
                    return true;
                }
                else
                {
                    return false;
                }
            }



        }

        private static void MakeBlankFile(string image)
        {
            using (MagickImage placeholder_image = new MagickImage(new MagickColor("#ff00ff"), 10, 10))
            {
                new Drawables()
                  .FillColor(MagickColor.FromRgba(123, 131, 235, 255))
                  .Rectangle(0, 0, 10, 10)
                  .Draw(placeholder_image);

                placeholder_image.Write(image);
            }
        }


        private static void MakeBlankMovie(string mov)
        {

            var inputFile = new MediaFile { Filename = mov };

            string ext = new FileInfo(mov).Extension;

            string tempfilename = mov.Replace(ext, "_temp" + ext);

            var outputFile = new MediaFile { Filename = tempfilename };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);

                var options = new ConversionOptions();

                options.VideoSize = VideoSize.Cga;


                // This example will create a 25 second video, starting from the 
                // 30th second of the original video.
                //// First parameter requests the starting frame to cut the media from.
                //// Second parameter requests how long to cut the video.
                options.CutMedia(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1));

                engine.Convert(inputFile, outputFile, options);
            }

            //delete old file
            File.Delete(mov);
            //rename the converted file
            File.Move(tempfilename, mov);
            //delete temp file
            File.Delete(tempfilename);

        }

        private static void MakeBlankMusic(string music)
        {

            var inputFile = new MediaFile { Filename = music };

            string ext = new FileInfo(music).Extension;

            string tempfilename = music.Replace(ext, "_temp" + ext);

            var outputFile = new MediaFile { Filename = tempfilename };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);

                var options = new ConversionOptions();

                //options.VideoSize = VideoSize.Cga;
                options.AudioSampleRate = AudioSampleRate.Hz22050;


                // This example will create a 25 second video, starting from the 
                // 30th second of the original video.
                //// First parameter requests the starting frame to cut the media from.
                //// Second parameter requests how long to cut the video.
                options.CutMedia(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1));

                engine.Convert(inputFile, outputFile, options);
            }

            //delete old file
            File.Delete(music);
            //rename the converted file
            File.Move(tempfilename, music);
            //delete temp file
            File.Delete(tempfilename);

        }

        private static Metafile MakeMetafile(float width, float height, string filename)
        {
            // Make a reference bitmap.
            using (Bitmap bm = new Bitmap(12, 12))
            {
                using (Graphics gr = Graphics.FromImage(bm))
                {
                    RectangleF bounds =
                        new RectangleF(0, 0, width, height);

                    Metafile mf;

                    mf = new Metafile(filename, gr.GetHdc(), bounds, MetafileFrameUnit.Pixel);

                    gr.ReleaseHdc();
                    return mf;
                }
            }
        }

        private static void DrawOnMetafile(Metafile mf)
        {
            using (Graphics gr = Graphics.FromImage(mf))
            {
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                gr.FillRectangle(new SolidBrush(Color.FromArgb(255, 123, 131, 235)), 0, 0, 12, 12);

            }
        }



        //Rehydration worker functions
        private void Rehydrate_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int steps = 0;
            int maxsteps = 5;
            string srcfile = PathOrginal.Text;
            string trafile = PathTranslated.Text;

            srcfile = srcfile.Replace("\"", "");
            trafile = trafile.Replace("\"", "");

            string srcDirectoryName = new FileInfo(srcfile).DirectoryName;
            string srcAbsPathTofile = new FileInfo(srcfile).FullName;
            string srcNameOfFile = new FileInfo(srcfile).Name;
            string srcFileExt = new FileInfo(srcfile).Extension;
            string srcUnzipDestinationDir = srcAbsPathTofile.Replace(srcFileExt, "");

            string traDirectoryName = new FileInfo(trafile).DirectoryName;
            string traAbsPathTofile = new FileInfo(trafile).FullName;
            string traNameOfFile = new FileInfo(trafile).Name;
            string traFileExt = new FileInfo(trafile).Extension;
            string traUnzipDestinationDir = traAbsPathTofile.Replace(traFileExt, "");

            string app = "";

            switch (srcFileExt.ToLower())
            {
                case ".pptx":
                    app = "ppt";
                    break;
                case ".xlsx":
                    app = "xl";
                    break;
                case ".xlsm":
                    app = "xl";
                    break;
                case ".docx":
                    app = "word";
                    break;
                default:
                    break;
            }

            string tempSrcDir = Path.Combine(srcDirectoryName, $"{srcUnzipDestinationDir}_src_temp");
            string tempTraDir = Path.Combine(traDirectoryName, $"{traUnzipDestinationDir}_tra_temp");
            Debug.WriteLine($"{tempSrcDir}\n{tempTraDir}");

            //unzip source
            if (Directory.Exists(tempSrcDir))
            {
                Directory.Delete(tempSrcDir,true);
            }
            

            ZipFile.ExtractToDirectory(srcAbsPathTofile, tempSrcDir);
            
            rehydrate_oWorker.ReportProgress(Percentage(steps++, maxsteps));
            //unzip translated


            if (Directory.Exists(tempTraDir))
            {
                Directory.Delete(tempTraDir, true);
            }
            ZipFile.ExtractToDirectory(traAbsPathTofile, tempTraDir);
            rehydrate_oWorker.ReportProgress(Percentage(steps++, maxsteps));


            //Delete the Translation Media Directory
            if (Directory.Exists(Path.Combine(tempTraDir, app, "media")) == true)
            {
                Directory.Delete(Path.Combine(tempTraDir, app, "media"),true);
            }
            //move the Original Media Directory to Translation Directory
            rehydrate_oWorker.ReportProgress(Percentage(steps++, maxsteps));

            if (Directory.Exists(Path.Combine(tempSrcDir, app, "media")) == true)
            {
                Directory.Move(Path.Combine(tempSrcDir, app, "media"), Path.Combine(tempTraDir, app,"media"));
            }
            rehydrate_oWorker.ReportProgress(Percentage(steps++, maxsteps));

            if (Directory.Exists(Path.Combine(srcDirectoryName, "Done")) == false)
            {
                Directory.CreateDirectory(Path.Combine(srcDirectoryName, "Done"));
            }

            ZipFile.CreateFromDirectory(tempTraDir, Path.Combine(srcDirectoryName, $"[Finalized]_{traNameOfFile}"));
            
            

            if (File.Exists(Path.Combine(srcDirectoryName,"Done", $"[Finalized]_{srcNameOfFile}")) == true)
            {
                File.Delete(Path.Combine(srcDirectoryName, "Done", $"[Finalized]_{srcNameOfFile}"));
            }

            

            File.Move(Path.Combine(srcDirectoryName, $"[Finalized]_{traNameOfFile}"),Path.Combine(srcDirectoryName, "Done", $"[Finalized]_{srcNameOfFile}"));
            rehydrate_oWorker.ReportProgress(Percentage(steps++, maxsteps));


            

            //clean-up
            if (Directory.Exists(tempSrcDir) == true)
            {
                Directory.Delete(tempSrcDir,true);
            }

            if (Directory.Exists(tempTraDir) == true)
            {
                Directory.Delete(tempTraDir,true);
            }
            rehydrate_oWorker.ReportProgress(Percentage(steps++, maxsteps));


        }
        private void Rehydrate_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        void Rehydrate_Completed(object sender, RunWorkerCompletedEventArgs e)
        {

            MessageBox.Show("Done");
            progressBar.Value = 0;

            MainOptionGroup.Enabled = true;

            SelectDehydrateFile.Enabled = false;
            SelectOrgSrcFile.Enabled = true;
            SelectFileWithTranslations.Enabled = true;
            PathReduce.Enabled = false;
            DehydrateButton.Enabled = false;

            ActionGroup.Enabled = false;
            QualityGroup.Enabled = false;

            PathOrginal.Enabled = true;
            PathTranslated.Enabled = true;
            RehydrateButton.Enabled = true;

            string @filename = PathOrginal.Text;
            string Absfilename = filename.Replace("\"", "");
            string directoryName = new FileInfo(Absfilename).DirectoryName;

            Process.Start(Path.Combine(directoryName, "Done"));

        }
        
        //General utility
        private int Percentage(int currentcount, int maxcount)
        {
            int percentage = (currentcount) * 100 / maxcount;
            return percentage;
        }



        //Event-based functions
        private void OptionDehydrate_CheckedChanged(object sender, EventArgs e)
        {

            SelectDehydrateFile.Enabled = true;
            SelectOrgSrcFile.Enabled = false;
            SelectFileWithTranslations.Enabled = false;
            PathReduce.Enabled = true;
            DehydrateButton.Enabled = true;


            ActionGroup.Enabled = true;

            if (ActionKeepNOImages.Checked == true)
            {
                QualityGroup.Enabled = false;
            }
            else
            {
                QualityGroup.Enabled = true;
            }

            PathOrginal.Enabled = false;
            PathTranslated.Enabled = false;
            RehydrateButton.Enabled = false;
        }

        private void OptionRehydrate_CheckedChanged(object sender, EventArgs e)
        {

            SelectDehydrateFile.Enabled = false;
            SelectOrgSrcFile.Enabled = true;
            SelectFileWithTranslations.Enabled = true;
            PathReduce.Enabled = false;
            DehydrateButton.Enabled = false;

            ActionGroup.Enabled = false;
            QualityGroup.Enabled = false;

            PathOrginal.Enabled = true;
            PathTranslated.Enabled = true;
            RehydrateButton.Enabled = true;

        }

        private void ActionKeepALLImages_CheckedChanged(object sender, EventArgs e)
        {
            QualityGroup.Enabled = true;
        }

        private void ActionKeepSOMEImages_CheckedChanged(object sender, EventArgs e)
        {
            QualityGroup.Enabled = true;
        }

        private void ActionKeepNOImages_CheckedChanged(object sender, EventArgs e)
        {
            QualityGroup.Enabled = false;
        }

        private void QualityGood_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void QualityBad_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void QualityUgly_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void SelectDehydrateFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Office files (*.docx;*.pptx;*.xlsx)|*.docx;*.pptx;*.xlsx;*.xlsm|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    PathReduce.Text = openFileDialog.FileName;
                }

            }
        }

        private void SelectOrgSrcFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Office files (*.docx;*.pptx;*.xlsx)|*.docx;*.pptx;*.xlsx;*.xlsm|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    PathOrginal.Text = openFileDialog.FileName;
                }

            }
        }

        private void SelectFileWithTranslations_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Office files (*.docx;*.pptx;*.xlsx)|*.docx;*.pptx;*.xlsx;*.xlsm|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    PathTranslated.Text = openFileDialog.FileName;
                }

            }
        }

        private void DehydrateButton_Click(object sender, EventArgs e)
        {
            MainOptionGroup.Enabled = false;


            SelectDehydrateFile.Enabled = false;
            SelectOrgSrcFile.Enabled = false;
            SelectFileWithTranslations.Enabled = false;
            PathReduce.Enabled = false;
            DehydrateButton.Enabled = false;


            ActionGroup.Enabled = false;


            QualityGroup.Enabled = false;


            PathOrginal.Enabled = false;
            PathTranslated.Enabled = false;
            RehydrateButton.Enabled = false;

            //Hack to prevent the user from sending an empty textbox
            try
            {
                string test = new FileInfo(PathReduce.Text).FullName;
                dehydrate_oWorker.RunWorkerAsync();
            }
            catch (Exception)
            {

                MessageBox.Show("Remember to choose a file to dehydrate!");
                MainOptionGroup.Enabled = true;
                SelectDehydrateFile.Enabled = true;
                SelectOrgSrcFile.Enabled = false;
                SelectFileWithTranslations.Enabled = false;
                PathReduce.Enabled = true;
                DehydrateButton.Enabled = true;


                ActionGroup.Enabled = true;

                if (ActionKeepNOImages.Checked == true)
                {
                    QualityGroup.Enabled = false;
                }
                else
                {
                    QualityGroup.Enabled = true;
                }

                PathOrginal.Enabled = false;
                PathTranslated.Enabled = false;
                RehydrateButton.Enabled = false;
            }


        }

        private void RehydrateButton_Click(object sender, EventArgs e)
        {
            MainOptionGroup.Enabled = false;
            SelectDehydrateFile.Enabled = false;
            SelectOrgSrcFile.Enabled = false;
            SelectFileWithTranslations.Enabled = false;
            PathReduce.Enabled = false;
            DehydrateButton.Enabled = false;
            ActionGroup.Enabled = false;
            QualityGroup.Enabled = false;
            PathOrginal.Enabled = false;
            PathTranslated.Enabled = false;
            RehydrateButton.Enabled = false;



            bool passTest1 = false;
            bool passTest2 = false;

            if (File.Exists(PathOrginal.Text))
            {
                string test1 = new FileInfo(PathOrginal.Text).FullName;
                passTest1 = true;
            }

            if (File.Exists(PathTranslated.Text))
            {
                string test1 = new FileInfo(PathTranslated.Text).FullName;
                passTest2 = true;
            }

            

            if (passTest1 == false && passTest2 == true)
            {
                MessageBox.Show("Remember to choose the original hand-off file");
            }
            if (passTest1 == true && passTest2 == false)
            {
                MessageBox.Show("Remember to choose the translated file");
            }

            if (passTest1 == false && passTest2 == false)
            {
                MessageBox.Show("Remember to choose both original and translated files");
            }

            if (passTest1 == false || passTest2 == false)
            { 
                MainOptionGroup.Enabled = true;
                SelectDehydrateFile.Enabled = false;
                SelectOrgSrcFile.Enabled = true;
                SelectFileWithTranslations.Enabled = true;
                PathReduce.Enabled = false;
                DehydrateButton.Enabled = false;
                ActionGroup.Enabled = false;
                PathOrginal.Enabled = true;
                PathTranslated.Enabled = true;
                RehydrateButton.Enabled = true;

            }

            if (passTest1 == true && passTest2 == true)
            {
                Debug.WriteLine($"rehydrate_oWorker.RunWorkerAsync()");
                rehydrate_oWorker.RunWorkerAsync();

            }







        }

    }

    public class ImageSettings
    {
        public int size;
        public int compression_gif;
        public int compression_jpg;
        public int compression_png;
        public int compression_tif;
        public ImageSettings()
        {
            this.size = 1;
            this.compression_gif = 1;
            this.compression_jpg = 1;
            this.compression_png = 1;
            this.compression_tif = 1;
        }
    }
}
